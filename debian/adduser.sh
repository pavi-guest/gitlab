#!/bin/sh

# add gitlab user (requires adduser >= 3.34)
# don't muck around with this unless you KNOW what you're doing

# Take gitlab_user from envornment variable or use gitlab
test -n "${gitlab_user}" || gitlab_user="gitlab"

# Create gitlab user with home in /var/lib
echo "Creating/updating ${gitlab_user} user account..."
adduser --system --home /var/lib/gitlab --gecos "${gitlab_user} user" --shell /bin/sh \
	--quiet --disabled-password --group ${gitlab_user} || {
echo "Proceeding with existing ${gitlab_user} user..."
  }
