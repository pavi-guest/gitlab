ucount=$(ls -1a |grep -vx .git | wc -l)
dcount=$(cat debian/gitlab.install |cut -d' ' -f1|grep -v debian |wc -l)
ignored=32
if ! [ $(echo "$ucount" - "$dcount"|bc) -eq $ignored ]; then
  echo "Found new files added by upstream and not added to debian/install"
  echo "Add them to debian/gitlab.install or adjust 'ignored=${ignored}'"
  echo "in debian/upstream-file-count-check.sh as required"
  exit 1
fi
